import "easymde/dist/easymde.min.css";

import EasyMDE from "easymde";

const easymde = {};

document.querySelectorAll(".easymde").forEach((e) => {
  easymde[e] = new EasyMDE({
    element: e,
    spellChecker: false,
  });
});
