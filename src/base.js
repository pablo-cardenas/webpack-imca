import "./style.scss";
import "../node_modules/dark-mode-toggle/src/dark-mode-toggle.mjs";

const menuBtn = document.querySelector(".menu-icon");
const hamburger = document.querySelector(".menu-icon__burger");
const nav = document.querySelector(".navbar");
const menuNav = document.querySelector(".navbar__list");
const navItems = document.querySelectorAll(".navbar__item");

let showMenu = false;

menuBtn.addEventListener("click", () => {
  if (!showMenu) {
    hamburger.classList.add("open");
    nav.classList.add("open");
    menuNav.classList.add("open");
    navItems.forEach((item) => {
      item.classList.add("open");
    });
    showMenu = true;
  } else {
    hamburger.classList.remove("open");
    nav.classList.remove("open");
    menuNav.classList.remove("open");
    navItems.forEach((item) => {
      item.classList.remove("open");
    });
    showMenu = false;
  }
});
